Feature: Открытие страницы игроков

    Scenario: Базовый, открытие по прямому урлу.

      Then Open "user/player/admin" page
      Then Current Url "user/player/admin" is expected?
      Then Button createPlayer is visible?
      Then element playerManagementGrid is visible?


    Scenario: Откртие со страницы дашборда, с главной панели

      Then Open "configurator/dashboard/index" page
      Then Current Url "configurator/dashboard/index" is expected?
      Then Content "admin1" is visible?
      Then dashboard elements is visible?
      Then Click panelMiniBoxUserPlayer Link
      Then Current Url "user/player/admin" is expected?
      Then Button createPlayer is visible?
      Then element playerManagementGrid is visible?

    Scenario: Откртие со страницы дашборда, с "бургер-меню"

      Then Open "configurator/dashboard/index" page
      Then Current Url "configurator/dashboard/index" is expected?
      Then Content "admin1" is visible?
      Then dashboard elements is visible?

      Then Click burgerMenuUserPlayer Link
      Then Current Url "user/player/admin" is expected?
      Then Button createPlayer is visible?
      Then element playerManagementGrid is visible?