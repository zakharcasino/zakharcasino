Feature: Сортировка грида игроков

    Scenario: Базовый, открытие по прямому урлу.

      #todo: чтобы полноценно убедиться в коррректности сортировки, мы заведомо должны знать на каких данных тестим,
      # тут или статичный дамп БД подставлять,
      # или тест фреймворк должен предварительно генерить под наш тест-кейс данные и мы этим могли бы управлять.
      # если этого не делать тест будет с рандом фейлами, когда данные на хосте могут меняться

      Then Open "user/player/admin" page
      Then Current Url "user/player/admin" is expected?
      Then Button createPlayer is visible?
      Then element playerManagementGrid is visible?
      Then Click registrationDateColumn button
      Then Current Url "user/player/admin?PlayerSearch_sort=registration_time" is expected?
      Then element registrationDateColumnSortingAsc is visible?
      Then date in first is older than next line?



