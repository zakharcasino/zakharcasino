Feature: Авторизоваться в админке

    Scenario: Позитивный, успешная авторизация.

      Then Click "#UserLogin_username" button
      Then Input login
      Then Click "#UserLogin_password" button
      Then Input password
      Then element "#UserLogin_verifyCode" is not visible?
      Then Click SignIn button
      Then Current Url "configurator/dashboard/index" is expected?
      Then Content "admin1" is visible?
      Then dashboard elements is visible?