package ApiTests;

import ApiTests.spec.Specification;
import config.EnvConf;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class ApiHelper {

        public static String getAuthenticationBearerToken(){
            Specification.installSpecification(Specification.requestSpec(EnvConf.BASE_API_URL), Specification.responseSpecRequestStatusCode(200));
            Map<String, String> user = new HashMap<>();
            user.put("grant_type", "client_credentials");
            user.put("username", "guest");
            user.put("password", "default");
            Response response = given()
                    .body(user)
                    .when()
                    .post("v2/oauth2/token")
                    .then().log().all()
                    .body("token_type", equalTo("Bearer"))
                    .body("expires_in", notNullValue())
                    .body("access_token", notNullValue())
                    .extract().response();

            JsonPath jsonPath = response.jsonPath();
            return jsonPath.get("access_token");
        }
}
