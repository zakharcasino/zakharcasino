package ApiTests.test.credentials;

import ApiTests.spec.Specification;
import config.UserConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static config.EnvConf.BASE_API_URL;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;


public class CredentialsTest {

    @Test
    public void getClientCredentialsGrant() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequest200());
        Map<String, String> user = new HashMap<>();
        user.put("grant_type", "client_credentials");
        user.put("scope", UserConfig.API_CLIENT_CREDENTIALS_GRANT);
        Response response = given()
                .body(user)
                .when()
                .post("v2/oauth2/token")
                .then().log().all()
                .body("token_type", equalTo("Bearer"))
                .body("expires_in", notNullValue())
                .body("access_token", notNullValue())
                .extract().response();
    }

    @Test
    public void ownerPasswordCredentialsGrant() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequest200());
        Map<String, String> user = new HashMap<>();
        user.put("grant_type", "client_credentials");
        user.put("username", "guest");
        user.put("password", "default");
        Response response = given()
                .body(user)
                .when()
                .post("v2/oauth2/token")
                .then().log().all()
                .body("token_type", equalTo("Bearer"))
                .body("expires_in", notNullValue())
                .body("access_token", notNullValue())
                .extract().response();

        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals("Bearer", jsonPath.get("token_type"));

    }

}
