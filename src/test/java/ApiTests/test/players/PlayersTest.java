package ApiTests.test.players;

import ApiTests.spec.Specification;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import ApiTests.test.players.data.PlayersData;

import java.util.HashMap;
import java.util.Map;

import static config.EnvConf.BASE_API_URL;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static ApiTests.ApiHelper.getAuthenticationBearerToken;


public class PlayersTest {

    /**
     * тут не удалось 200 ОК добиться, вероятно криво передаю права:
     *             user.put("grant_type", "client_credentials");
     *             user.put("username", "guest");
     *             user.put("password", "default");
     */
    @Test
    public void registrationPlayer() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequest200());
        given()
                .headers(
                        "Authorization",
                        "Bearer " + getAuthenticationBearerToken()
                )
                .when()
                .get("v2/players")
                .then().log().all();
    }

    /**
     * не удалось 200 ОК добиться, вероятно криво передаю права для BearerToken.
     */
    @Test
    public void createPlayer() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequestStatusCode(201));
        Map<String, String> player = new HashMap<>();
        player.put("username", "janedoe");
        player.put("password_change", "amFuZWRvZTEyMw==");
        player.put("password_repeat", "amFuZWRvZTEyMw==");
        player.put("email", "janedoe@example.com");
        player.put("surname", "Doe");
        player.put("currency_code", "EUR");
        Response response = given()
                .headers(
                        "Authorization",
                        "Bearer " + getAuthenticationBearerToken()
                )
                .when()
                .post("v2/players")
                .then().log().all()
                .body("gender", nullValue())
                .body("phone_number", nullValue())
                .body("birthdate", nullValue())
                .extract().response();

        JsonPath jsonPath = response.jsonPath();
        Assert.assertEquals("1", jsonPath.get("country_id"));
        Assert.assertEquals("janedoe", jsonPath.get("username"));
        Assert.assertEquals("janedoe@example.com", jsonPath.get("email"));
        Assert.assertEquals("John", jsonPath.get("name"));
        Assert.assertEquals("Doe", jsonPath.get("surname"));
        Assert.assertEquals("true", jsonPath.get("bonuses_allowed"));
        Assert.assertEquals("true", jsonPath.get("is_verified"));

    }

    /**
     * Пример использования lombok.Data
     *
     * не удалось 200 ОК добиться, отпадаю по 401.
     */
    @Test
    public void getSinglePlayerProfile() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequest200());
        String userId = "1";
        PlayersData players = given()
                .when()
                .get("v2/players/" + userId)
                .then().log().all()
                 .extract().as(PlayersData.class);

        Assert.assertEquals(userId, players.getId());
        Assert.assertEquals("johndoe", players.getUsername());
        Assert.assertEquals("+1234567891", players.getPhone_number());

    }

    /**
     * стоит уточнить тут "другой игрок" обязательно айди с числами или может быть какой-то строковый хеш
     * добавить кейсов с отсутсвием обязательного параметра айди
     * не удалось 200 ОК добиться, отпадаю по 401.
     */
    @Test
    public void getSinglePlayerProfileNegative() {
        Specification.installSpecification(Specification.requestSpec(BASE_API_URL), Specification.responseSpecRequestStatusCode(404));
        String userId = "12345";
        given()
                .when()
                .get("v2/players/" + userId)
                .then().log().all()
                .body("message", equalTo("Page not found"));
    }
}
