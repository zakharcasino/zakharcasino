package steps;

import io.cucumber.java.en.Then;
import pages.BasicHelper;
import pages.ConfiguratorDashboardPage;

public class ConfiguratorDashboardPageDefs {

    @Then("Click panelMiniBoxUserPlayer Link")
    public void clickPanelMiniBoxUserPlayerLink() {
        BasicHelper.clickElement(ConfiguratorDashboardPage.panelMiniBoxUserPlayerLink);
    }

    @Then("Click burgerMenuUserPlayer Link")
    public void clickBurgerMenuUserPlayerLink() {
        BasicHelper.clickElement(ConfiguratorDashboardPage.burgerMenuUser);
        BasicHelper.clickElement(ConfiguratorDashboardPage.burgerMenuUserPlayerLink);
    }
}
