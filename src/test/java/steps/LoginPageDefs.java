package steps;

import config.UserConfig;
import io.cucumber.java.en.Then;
import pages.ConfiguratorDashboardPage;
import pages.LoginPage;


public class LoginPageDefs {

    ConfiguratorDashboardPage configuratorDashboardPage = new ConfiguratorDashboardPage();

    LoginPage loginPage = new LoginPage();
    @Then("Input login")
    public void inputLogin() {
        loginPage.loginInput(UserConfig.USER_LOGIN);
    }

    @Then("Input password")
    public void inputPassword() {
        loginPage.passwordInput(UserConfig.USER_PASSWORD);
    }

    @Then("Click SignIn button")
    public void clickSignInButton() {
        loginPage.submitSignInButton();
    }

    @Then("text error {string} blank field is visible?")
    public void textErrorBlankFieldIsVisible(String arg0) {
        loginPage.textErrorBlankFieldIsVisible(arg0);
    }

    @Then("dashboard elements is visible?")
    public void dashboardElementsIsVisible() {
        configuratorDashboardPage.checkDashboardElementsIsVisible();
    }
}
