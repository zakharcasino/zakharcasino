package steps;

import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeOptions;
import pages.LoginPage;

import static com.codeborne.selenide.Selenide.open;

public class Hooks {

    @Before
    public void initWebDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("start-maximized");
        options.addArguments("--window-size=1920,1080");
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        open(LoginPage.ROUTE_URL_LOGIN);
    }
}
