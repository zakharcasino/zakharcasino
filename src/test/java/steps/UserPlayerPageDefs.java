package steps;

import io.cucumber.java.en.Then;
import pages.BasicHelper;
import pages.UserPlayerPage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static pages.UserPlayerPage.getElementPlayerGrid;

public class UserPlayerPageDefs {

    @Then("Button createPlayer is visible?")
    public void buttonCreatePlayerIsVisible() {
        BasicHelper.checkElementVisible(
                UserPlayerPage.createPlayerButton
        );
    }

    @Then("element playerManagementGrid is visible?")
    public void elementPlayerManagementGridIsVisible() {
        BasicHelper.checkElementVisible(
                UserPlayerPage.playerManagementGrid
        );
    }

    @Then("Click registrationDateColumn button")
    public void clickRegistrationDateColumnButton() {
        BasicHelper.clickElement(UserPlayerPage.registrationDateColumn);
    }



    @Then("element registrationDateColumnSortingAsc is visible?")
    public void elementRegistrationDateColumnSortingAscIsVisible() {
        BasicHelper.checkElementVisible(
                UserPlayerPage.registrationDateColumnSortingAsc
        );
    }

    @Then("date in first is older than next line?")
    public void dateInFirstIsOlderThanNextLine() throws ParseException {
        String dateFirstLine = UserPlayerPage.getValueElementsPlayerGrid(
                getElementPlayerGrid(1, 10)
        );
        String dateSecondLine = UserPlayerPage.getValueElementsPlayerGrid(
                getElementPlayerGrid(2, 10)
        );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date registrationDateFirstLine = sdf.parse(dateFirstLine);
        Date registrationDateSecondLine = sdf.parse(dateSecondLine);

        assertEquals("чек корректной сортировки по дате", true, registrationDateFirstLine.before(registrationDateSecondLine));
    }
}
