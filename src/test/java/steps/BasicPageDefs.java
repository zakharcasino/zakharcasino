package steps;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import pages.BasicHelper;

import static com.codeborne.selenide.Selenide.$;

public class BasicPageDefs {

    BasicHelper basicPage = new BasicHelper();

    @Then("Click {string} button")
    public void clickButton(String arg0) {
        basicPage.clickElementByCssSelector(arg0);
    }

    @Then("Content {string} is visible?")
    public void contentIsVisible(String arg0) {
       $(By.xpath("//*[contains(text(),'" + arg0 + "')]")).shouldBe(Condition.visible);
    }

    @Then("Current Url {string} is expected?")
    public void currentUrlIsExpected(String arg0) {
        basicPage.checkCurrentURL(arg0);
    }

    @Then("element {string} is visible?")
    public void elementIsVisible(String arg0) {
        $(By.cssSelector(arg0)).shouldBe(Condition.visible);
    }

    @Then("element {string} is not visible?")
    public void elementIsNotVisible(String arg0) {
        $(By.cssSelector(arg0)).shouldNotBe(Condition.visible);
    }

    @Then("Open {string} page")
    public void openPage(String arg0) {
        basicPage.openPage(arg0);
    }
}
