package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import config.EnvConf;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class UserPlayerPage {

    public static final String ROUTE_URL_USER_PLAYER = EnvConf.BASE_URL + "user/player/admin";

    public static final SelenideElement createPlayerButton = $(By.xpath("//*[@class='btn btn-info'][@href='/user/player/create']"));
    public static final SelenideElement playerManagementGrid = $(By.cssSelector("#payment-system-transaction-grid"));

    //Grid
    public static final SelenideElement registrationDateColumn = $(By.xpath("//a[contains(text(),'Registration date')]"));

    //Sorting
    public static final SelenideElement registrationDateColumnSortingAsc = $(By.xpath("//a[contains(text(),'Registration date')][@class='sort-link asc']"));


    /**
     * Оптимизировать, сбособ не надежен, при изменении струтуры грида, селекторы все "поплывут"
     */
    public static SelenideElement getElementPlayerGrid(int line, int column) {
        return $(By.xpath("//tbody/tr[" + line + "]/td[" + column + "]"));
    }

    public static String getValueElementsPlayerGrid(SelenideElement element){
        return element.shouldBe(Condition.visible).getText();
    }

}
