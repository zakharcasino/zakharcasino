package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import config.EnvConf;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    public static final String ROUTE_URL_LOGIN = EnvConf.BASE_URL + "admin/login";

    private final SelenideElement loginInput = $(By.cssSelector("#UserLogin_username"));
    private final SelenideElement passwordInput = $(By.cssSelector("#UserLogin_password"));
    private final SelenideElement loginCaptchaInput = $(By.cssSelector("#UserLogin_verifyCode"));

    private final SelenideElement errorPasswordInputBlank = $(By.xpath("//div[@class='errorMessage'][text()='Password cannot be blank.']"));
    private final SelenideElement errorCaptchaInputBlank = $(By.xpath("//div[@class='errorMessage'][text()='Verification Code cannot be blank.']"));
    private final SelenideElement submitButton = $(By.xpath("//*[@class='btn btn-primary btn-lg btn-block']")); //уточнить не динамический ли //*[@name='yt0']

    public void loginInput(String text) {
        this.loginInput.val(text);
    }

    public void passwordInput(String text) {
        this.passwordInput.val(text);
    }

    public void submitSignInButton() {
        submitButton.click();
    }

    public void loginCaptchaInput(String text) {
        this.loginCaptchaInput.val(text);
    }

    public void textErrorBlankFieldIsVisible(String errorText) {
        $(By.xpath("//div[@class='errorMessage'][text()='" + errorText + "']")).shouldBe(Condition.visible);
    }
}
