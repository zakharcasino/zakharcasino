package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ConfiguratorDashboardPage extends BasicHelper {

    private final SelenideElement withdrawalsChart = $(By.cssSelector("#withdrawals_chart"));
    private final SelenideElement depositsChart = $(By.cssSelector("#deposits_chart"));
    public static final SelenideElement panelMiniBoxUserPlayerLink = $(By.xpath("//*[@href='/user/player/admin']//*[@class='panel mini-box']"));
    public static final SelenideElement burgerMenuUserPlayerLink = $(By.xpath("//*[@id='s-menu-users']//*[@href='/user/player/admin']"));
    public static final SelenideElement burgerMenuUser = $(By.xpath("//a[@data-target='#s-menu-users']"));

    public void checkDashboardElementsIsVisible() {
        BasicHelper.checkElementVisible(withdrawalsChart);
        BasicHelper.checkElementVisible(depositsChart);
    }
}
