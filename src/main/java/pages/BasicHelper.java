package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import config.EnvConf;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertEquals;


public class BasicHelper {
    public void clickElementByCssSelector(String element) {
         $(By.cssSelector(element)).click();
    }

    public static void clickElement(SelenideElement element) {
        element.shouldBe(Condition.visible).click();
    }

    /**
     * String url передавать со слешом в конце
    */
    public void openPage(String url) {
        open(EnvConf.BASE_URL + url);
    }

    public void checkCurrentURL(String expectedUrl) {
        String currentUrl = url();
        assertEquals("Текущий URL " + currentUrl + " не совпадает с ожидаемым",  EnvConf.BASE_URL + expectedUrl, currentUrl);
    }

    public static void checkElementVisible(SelenideElement element) {
        element.shouldBe(Condition.visible);
    }
}
